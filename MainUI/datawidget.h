#ifndef DATAWIDGET_H
#define DATAWIDGET_H

#include <QWidget>
#include <QDateTime>
#include <QDir>
#include <QByteArray>

#include "setting.h"

namespace Ui {
class DataWidget;
}

class DataWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DataWidget(QWidget *parent = nullptr);
    ~DataWidget();

private:
    Ui::DataWidget *ui;
    Setting *pSetting;

    QString dateTime;
    QString savePath;

    int channelCount;

private:

    ///
    /// \brief initListWidget初始化导航列表
    ///
    void initListWidget();

    ///
    /// \brief initSavePath创建保存图片文件夹
    ///
    void initSavePath();

public slots:

    ///
    /// \brief serialLogicStatus逻辑状态
    /// \param status
    ///
    void serialLogicStatus(int *status, int channel);

    ///
    /// \brief setPutPicture逻辑抓拍显示图片
    /// \param stream图片流
    /// \param put抓拍指令
    /// \param id相机ID
    ///
    void setPutPicture(const QByteArray &stream,int labelID, int id);

    ///
    /// \brief clearnLabel清除图片
    ///
    void clearnLabel(int channnel);
};

#endif // DATAWIDGET_H
