#include "datawidget.h"
#include "ui_datawidget.h"

DataWidget::DataWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DataWidget)
{
    ui->setupUi(this);

    pSetting=new  Setting();//加载配置

    this->setParent(parent);
    setWindowFlags(Qt::CustomizeWindowHint|Qt::FramelessWindowHint);

    this->initListWidget();
    this->initSavePath();
}

DataWidget::~DataWidget()
{
    delete pSetting;
    delete ui;
}

void DataWidget::initListWidget()
{
    QStringList list;

    channelCount=pSetting->pSetting->value("MAIN/ChannelNumber").toInt();

    for(int i=1;i<channelCount;i++)
    {
        list.append(QString(tr("通道%1").arg(i+1)));
    }
    ui->listWidget->addItems(list);
}

void DataWidget::initSavePath()
{
    savePath=pSetting->pSetting->value("MAIN/ImagePath").toString();

    for(int i=1;i<=channelCount;i++){
        QString imgPath=tr("%1%2").arg(savePath).arg(i);
        QDir path(imgPath);
        if(!path.exists())
        {
            path.mkpath(imgPath);
        }
    }
}

void DataWidget::serialLogicStatus(int* status, int channel)
{    
    switch (channel) {
       case 1:
           ui->checkBox->setChecked(status[0]?true:false);
           ui->checkBox_4->setChecked(status[1]?true:false);
           ui->checkBox_2->setChecked(status[3]?true:false);
           ui->checkBox_5->setChecked(status[4]?true:false);
           ui->checkBox_3->setChecked(status[2]?true:false);
           break;
       case 2:
           ui->checkBox_6->setChecked(status[0]?true:false);
           ui->checkBox_7->setChecked(status[1]?true:false);
           ui->checkBox_8->setChecked(status[3]?true:false);
           ui->checkBox_9->setChecked(status[4]?true:false);
           ui->checkBox_10->setChecked(status[2]?true:false);
           break;
       case 3:
           ui->checkBox_11->setChecked(status[0]?true:false);
           ui->checkBox_12->setChecked(status[1]?true:false);
           ui->checkBox_13->setChecked(status[3]?true:false);
           ui->checkBox_14->setChecked(status[4]?true:false);
           ui->checkBox_15->setChecked(status[2]?true:false);
           break;
       case 4:
           ui->checkBox_16->setChecked(status[0]?true:false);
           ui->checkBox_17->setChecked(status[1]?true:false);
           ui->checkBox_18->setChecked(status[3]?true:false);
           ui->checkBox_19->setChecked(status[4]?true:false);
           ui->checkBox_20->setChecked(status[2]?true:false);
           break;
       default:
           break;
    }
}

void DataWidget::setPutPicture(const QByteArray &stream, int labelID, int id)
{
    QPixmap *labelPix = new QPixmap();
    labelPix->loadFromData(stream);

    if(10<=id && id<=14)
    {
        //保存图片
        labelPix->save(tr("%1%2/%3%4%5.jpg").arg(savePath).arg(1).arg(1).arg(dateTime).arg(labelID));

        switch (labelID) {
        case 1:
            ui->label->setPixmap(*labelPix);
            break;
        case 2:
            ui->label_2->setPixmap(*labelPix);
            break;
        case 3:
            ui->label_5->setPixmap(*labelPix);
            break;
        case 4:
            ui->label_6->setPixmap(*labelPix);
            break;
        case 5:
            ui->label_3->setPixmap(*labelPix);
            break;
        case 6:
            ui->label_4->setPixmap(*labelPix);
            break;
        }
    }
    if(20<=id && id<=24)
    {
        //保存图片
        labelPix->save(tr("%1%2/%3%4%5.jpg").arg(savePath).arg(2).arg(2).arg(dateTime).arg(labelID));

        switch (labelID) {
        case 1:
            ui->label_11->setPixmap(*labelPix);
            break;
        case 2:
            ui->label_12->setPixmap(*labelPix);
            break;
        case 3:
            ui->label_13->setPixmap(*labelPix);
            break;
        case 4:
            ui->label_14->setPixmap(*labelPix);
            break;
        case 5:
            ui->label_15->setPixmap(*labelPix);
            break;
        case 6:
            ui->label_16->setPixmap(*labelPix);
            break;
        }
    }
    if(30<=id && id<=34)
    {
        //保存图片
        labelPix->save(tr("%1%2/%3%4%5.jpg").arg(savePath).arg(3).arg(3).arg(dateTime).arg(labelID));

        switch (labelID) {
        case 1:
            ui->label_19->setPixmap(*labelPix);
            break;
        case 2:
            ui->label_20->setPixmap(*labelPix);
            break;
        case 3:
            ui->label_21->setPixmap(*labelPix);
            break;
        case 4:
            ui->label_22->setPixmap(*labelPix);
            break;
        case 5:
            ui->label_23->setPixmap(*labelPix);
            break;
        case 6:
            ui->label_24->setPixmap(*labelPix);
            break;
        }
    }
    if(40<=id && id<=44)
    {
        //保存图片
        labelPix->save(tr("%1%2/%3%4%5.jpg").arg(savePath).arg(4).arg(4).arg(dateTime).arg(labelID));

        switch (labelID) {
        case 1:
            ui->label_27->setPixmap(*labelPix);
            break;
        case 2:
            ui->label_28->setPixmap(*labelPix);
            break;
        case 3:
            ui->label_29->setPixmap(*labelPix);
            break;
        case 4:
            ui->label_30->setPixmap(*labelPix);
            break;
        case 5:
            ui->label_31->setPixmap(*labelPix);
            break;
        case 6:
            ui->label_32->setPixmap(*labelPix);
            break;
        }
    }

    delete labelPix;
}

void DataWidget::clearnLabel(int channnel)
{
    QDateTime time=QDateTime::currentDateTime();
    dateTime=time.toString("yyyyMMddhhmmss");

    switch (channnel) {
    case 1:
        ui->label->clear();
        ui->label_2->clear();
        ui->label_3->clear();
        ui->label_4->clear();
        ui->label_5->clear();
        ui->label_6->clear();
        break;
    case 2:
        ui->label_11->clear();
        ui->label_12->clear();
        ui->label_13->clear();
        ui->label_14->clear();
        ui->label_15->clear();
        ui->label_16->clear();
        break;
    case 3:
        ui->label_19->clear();
        ui->label_20->clear();
        ui->label_21->clear();
        ui->label_22->clear();
        ui->label_23->clear();
        ui->label_24->clear();
        break;
    case 4:
        ui->label_27->clear();
        ui->label_28->clear();
        ui->label_29->clear();
        ui->label_30->clear();
        ui->label_31->clear();
        ui->label_32->clear();
        break;
    }
}
