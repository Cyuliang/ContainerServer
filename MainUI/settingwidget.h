#ifndef SETTINGWIDGET_H
#define SETTINGWIDGET_H

#include <QWidget>
#include <QAbstractButton>
#include <QButtonGroup>
#include <QMessageBox>

#include "setting.h"

namespace Ui {
class SettingWidget;
}

class SettingWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SettingWidget(QWidget *parent = nullptr);
    ~SettingWidget();

    Setting *pSettings;

    ///
    /// \brief camerIDMap相机ID和地址
    ///
    QMap<int,QString> camerIDMap;

    ///
    /// \brief comPortList通道号和串口配置
    ///
    QMap<int,QVector<QString>>comPortList;

    ///
    /// \brief AlarmModel红外模式
    ///
    QMap<int,bool> AlarmModel;

private slots:

    ///
    /// \brief on_comboBox_currentIndexChanged添加通道列表
    /// \param index选取号
    ///
    void on_comboBox_currentIndexChanged(int index);

    ///
    /// \brief on_buttonBox_clicked保存参数
    /// \param button
    ///
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::SettingWidget *ui;

    ///
    /// \brief writeINI写入参数
    ///
    void writeINI();

    ///
    /// \brief readINI读取参数
    ///
    void readINI();

    ///
    /// \brief addComboxItem添加串口列表
    ///
    void addComboxItem();

    ///
    /// \brief initCamerIpList初始化相机ID和地址
    ///
    void initCamerIpList();

    ///
    /// \brief initComPortList初始化串口配置
    ///
    void initComPortList();
};

#endif // SettingWidget_H
