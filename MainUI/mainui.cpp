#include "mainui.h"
#include "ui_mainui.h"

MainUI::MainUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainUI)
{
    ui->setupUi(this);

    pThread=new QThread(this);

    pSetting=new Setting ();

    pLogic=new Logic (this);

    pDataWidget=new DataWidget (this);
    pDataWidget->move(72,80);

    pSettingWidget=new SettingWidget(this);
    pSettingWidget->move(72,80);

    pPictureWidget=new PictureWidget (this) ;
    pPictureWidget->move(72,80);       

    channelCount= pSetting->pSetting->value("MAIN/ChannelNumber").toInt();


    initSerialLogic();
    initGetImage();
    initConnected();
}

MainUI::~MainUI()
{
    pGetImageImgList.clear();
    pGetImagePutList.clear();

    pThread->terminate();

    delete pLogic;
    delete pSetting;
    delete pDataWidget;
    delete pSettingWidget;
    delete pPictureWidget;
    delete pThread;
    delete ui;
}

void MainUI::initConnected()
{
    foreach (GetImage* var, pGetImagePutList) {
        connect(var,&GetImage::message,this,&MainUI::message);
        connect(var,SIGNAL(camerStateSingal(int,bool)),pPictureWidget,SLOT(setCamerState(int,bool)));        
    }
    foreach (GetImage* var, pGetImageImgList) {
        connect(var,&GetImage::message,this,&MainUI::message);
        connect(var,SIGNAL(putStreamSignal(QByteArray)),pPictureWidget,SLOT(setLabelPictureSlot(QByteArray)));
        connect(var,SIGNAL(pictureStreamSignal(QByteArray,int,int)),pDataWidget,SLOT(setPutPicture(QByteArray,int,int)),Qt::QueuedConnection);
    }

    foreach (SerialLogic* var, pSerialLogicList) { 
        connect(var,&SerialLogic::message,this,&MainUI::message);
        connect(var,SIGNAL(logicStatus(int*,int,bool)),pDataWidget,SLOT(serialLogicStatus(int*,int)));
        connect(var,SIGNAL(logicStatus(int*,int,bool)),pLogic,SLOT(serialLogicStatus(int*,int,bool)));
    }

    connect(pPictureWidget,SIGNAL(sendCommand(int )),this,SLOT(sendCaptureCommandSlot(int)));

    connect(pLogic,&Logic::setLogicPutImage,this,&MainUI::LogicPutImage);

    connect(this,&MainUI::clearnLabel,pDataWidget,&DataWidget::clearnLabel);
}

void MainUI::initGetImage()
{
    GetImage  *pGetImagePut,*pGetImageImg;


    for(int i=0;i<channelCount*5;i++)
    {        
        if(pSettingWidget->camerIDMap.values()[i].toStdString()!="")
        {
            pGetImagePut=new GetImage();
            pGetImagePut->init(pSettingWidget->camerIDMap.values()[i],23000,pSettingWidget->camerIDMap.keys()[i]);
            pGetImagePutList[pSettingWidget->camerIDMap.keys()[i]]=pGetImagePut;

            pGetImageImg=new GetImage();
            pGetImageImg->init(pSettingWidget->camerIDMap.values()[i],23001,pSettingWidget->camerIDMap.keys()[i]);
            pGetImageImgList[pSettingWidget->camerIDMap.keys()[i]]=pGetImageImg;

            pGetImageImg->moveToThread(pThread);
            pThread->start();
        }
    }
}

void MainUI::initSerialLogic()
{
    SerialLogic* pSerialLogic;
    for(int i=0;i<channelCount;i++)
    {
        pSerialLogic =new SerialLogic ();

        pSerialLogic->setAlarmMode(pSettingWidget->AlarmModel.values()[i]);
        pSerialLogic->startSlave(pSettingWidget->comPortList.values()[i][0],pSettingWidget->comPortList.values()[i][1],pSettingWidget->comPortList.keys()[i]);

        pSerialLogicList[i]=pSerialLogic;
    }
}

void MainUI::sendCaptureCommandSlot(int id)
{
    if(pGetImagePutList[id])
    {
        pGetImageImgList[id]->setLabelID(0,0,0);
        pGetImagePutList[id]->PutCommand(tr("capture %1\n").arg(id));
    }
}

void MainUI::logicCaptureCommand(int id, int labelID,uint sleepTime)
{
    if(pGetImagePutList[id])
    {
        pGetImageImgList[id]->setLabelID(labelID,id,sleepTime);
        pGetImagePutList[id]->PutCommand(tr("capture %1\n").arg(id));
    }
}

void MainUI::LogicPutImage(int put, int channel)
{
    std::cout<<put<<"|"<<channel<<std::endl;
    int chan=channel*10;
    switch (put) {
    case -1:
        emit clearnLabel(channel);
        break;
    case 0:
         logicCaptureCommand(int(0+chan),1,100);
         logicCaptureCommand(int(2+chan),2,100);
         logicCaptureCommand(int(3+chan),3,100);
        break;
    case 1:
         logicCaptureCommand(int(1+chan),6,100);
         logicCaptureCommand(int(2+chan),4,100);
         logicCaptureCommand(int(3+chan),5,100);
        break;
    case 2:
         logicCaptureCommand(int(0+chan),1,100);
         logicCaptureCommand(int(1+chan),6,400);
         logicCaptureCommand(int(2+chan),2,200);
         logicCaptureCommand(int(3+chan),3,300);
        break;
    case 3:
         logicCaptureCommand(int(0+chan),1,100);
         logicCaptureCommand(int(2+chan),2,200);
         logicCaptureCommand(int(3+chan),3,300);
        break;
    case 4:
         logicCaptureCommand(int(1+chan),6,100);
         logicCaptureCommand(int(2+chan),5,200);
         logicCaptureCommand(int(3+chan),4,300);
        break;
    }
}

void MainUI::message(const QString &msg)
{
    std::cout<<msg.toStdString()<<std::endl;
}

void MainUI::resizeEvent(QResizeEvent *size)
{
    pDataWidget->resize( size->size().width()-72,size->size().height()-80);
    pSettingWidget->resize( size->size().width()-72,size->size().height()-80);
    pPictureWidget->resize( size->size().width()-72,size->size().height()-80);
}

void MainUI::on_listWidget_currentRowChanged(int currentRow)
{
    //pDataWidget->show();
    switch (currentRow) {
    case 0:
        //pDataWidget->setParent(this);
        pSettingWidget->hide();
        pPictureWidget->hide();
        pDataWidget->show();
        break;
    case 2:
        //pDataWidget->setParent(this);
        pSettingWidget->hide();
        pDataWidget->hide();
        pPictureWidget->show();
        break;
    case 4:
        //pSettingWidget->setParent(this);
        pDataWidget->hide();
        pPictureWidget->hide();
        pSettingWidget->show();
        break;
    }
}
