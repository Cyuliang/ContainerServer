#include "setting.h"

Setting::Setting()
{
    pSetting=new QSettings(QCoreApplication::applicationDirPath()+"/Setting.ini",QSettings::IniFormat);
    pSetting->setIniCodec("UTF-8");
}

Setting::~Setting()
{
    delete pSetting;
}
