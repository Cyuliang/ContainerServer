#-------------------------------------------------
#
# Project created by QtCreator 2019-10-19T13:39:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MainUI
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainui.cpp \
    datawidget.cpp \
    setting.cpp \
    picturewidget.cpp \
    settingwidget.cpp

HEADERS += \
        mainui.h \
    datawidget.h \
    setting.h \
    settingwidget.h \
    picturewidget.h

FORMS += \
        mainui.ui \
    datawidget.ui \
    picturewidget.ui \
    settingwidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    qrc/qrc.qrc \

DISTFILES += \
    qrc/run.svg \
    qrc/run_avtive.svg \
    qrc/equipment.svg \
    qrc/equipment_active.svg \
    qrc/run.svg \
    qrc/run_avtive.svg \
    qrc/find.svg \
    qrc/find_active.svg \
    qrc/setting_active.svg \
    qrc/setting.svg \
    qrc/ZBY.ico \
    qrc/back.svg \
    qrc/back_active.svg \
    qrc/ICO.ico

MOC_DIR=tmp/moc
RCC_DIR=tmp/rcc
UI_DIR=tmp/ui
OBJECTS_DIR=tmp/obj

QMAKE_LFLAGS += -Wl,-rpath,'$ORIGIN'

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../GetImage/release/ -lGetImage
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../GetImage/debug/ -lGetImage
else:unix:!macx: LIBS += -L$$OUT_PWD/../GetImage/ -lGetImage

INCLUDEPATH += $$PWD/../GetImage
DEPENDPATH += $$PWD/../GetImage

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../SerialLogic/release/ -lSerialLogic
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../SerialLogic/debug/ -lSerialLogic
else:unix:!macx: LIBS += -L$$OUT_PWD/../SerialLogic/ -lSerialLogic

INCLUDEPATH += $$PWD/../SerialLogic
DEPENDPATH += $$PWD/../SerialLogic

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../Logic/release/ -lLogic
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../Logic/debug/ -lLogic
else:unix:!macx: LIBS += -L$$OUT_PWD/../Logic/ -lLogic

INCLUDEPATH += $$PWD/../Logic
DEPENDPATH += $$PWD/../Logic
