#include "settingwidget.h"
#include "ui_settingwidget.h"

SettingWidget::SettingWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SettingWidget)
{
    ui->setupUi(this);

    pSettings=new Setting();
    this->addComboxItem();//添加串口列表
    this->readINI();//读取参数

    initCamerIpList();
    initComPortList();

    this->setParent(parent);
    setWindowFlags(Qt::CustomizeWindowHint|Qt::FramelessWindowHint);
    this->hide();    
}

SettingWidget::~SettingWidget()
{
    delete pSettings;
    delete ui;
}

void SettingWidget::on_comboBox_currentIndexChanged(int index)
{
    //-----删除列表------
    QListWidgetItem *item;
    int count=ui->listWidget->count();
    for(int j=1;j<count;j++)
    {
        item=ui->listWidget->takeItem(2);
        delete  item;
    }

    //------添加列表------
    QStringList list;
    for(int i=0;i<index;i++)
    {
        list.append(QString(tr("通道%1").arg(i+2)));
    }
    ui->listWidget->addItems(list);
}

void SettingWidget::writeINI()
{
    //MAIN----------
    pSettings->pSetting->beginGroup("MAIN");
    //通道号
    pSettings->pSetting->setValue("ChannelNumber",ui->comboBox->currentIndex()+1);

    //校验模式
    QButtonGroup *pQButtonGroup=new QButtonGroup (this);
    pQButtonGroup->addButton(ui->radioButton,0);
    pQButtonGroup->addButton(ui->radioButton_2,1);
    int check=pQButtonGroup->checkedId();
    pSettings->pSetting->setValue("DataCheck",check);
    pQButtonGroup->deleteLater();

    //协议模式
    pQButtonGroup=new QButtonGroup (this);
    pQButtonGroup->addButton(ui->radioButton_3,0);
    pQButtonGroup->addButton(ui->radioButton_4,1);
    check=pQButtonGroup->checkedId();
    pSettings->pSetting->setValue("AgreementType",check);
    pQButtonGroup->deleteLater();

    //服务器模式地址端口
    pSettings->pSetting->setValue("ServerIP",ui->lineEdit_10->text());
    pSettings->pSetting->setValue("ServerPort",ui->lineEdit_11->text());

    //客户端模式地址端口
    pSettings->pSetting->setValue("ClientIp",ui->lineEdit_12->text());
    pSettings->pSetting->setValue("ClientPort",ui->lineEdit_13->text());

    //图片保存路径
    pSettings->pSetting->setValue("ImagePath",ui->lineEdit_14->text());
    pSettings->pSetting->endGroup();
    //MAIN----------

    //FTP----------
    pSettings->pSetting->beginGroup("FTP");
    //地址
    pSettings->pSetting->setValue("Ip",ui->lineEdit_15->text());

    //端口
    pSettings->pSetting->setValue("Port",ui->lineEdit_16->text());

    //用户名
    pSettings->pSetting->setValue("Name",ui->lineEdit_17->text());

    //密码
    pSettings->pSetting->setValue("Password",ui->lineEdit_18->text());

    //本地图片路径
    pSettings->pSetting->setValue("LocalImagePath",ui->lineEdit_19->text());

    //远程图片路径
    pSettings->pSetting->setValue("RemoteImagePath",ui->lineEdit_20->text());
    pSettings->pSetting->endGroup();
    //FTP----------

    //channel1----------
    pSettings->pSetting->beginGroup("Channel1");
    pSettings->pSetting->setValue("Before",ui->lineEdit_48->text());//前
    pSettings->pSetting->setValue("After",ui->lineEdit_49->text());//后
    pSettings->pSetting->setValue("Left",ui->lineEdit_50->text());//左
    pSettings->pSetting->setValue("Right",ui->lineEdit_51->text());//右
    pSettings->pSetting->setValue("Plate",ui->lineEdit_52->text());//车牌
    pSettings->pSetting->setValue("ComPortType",ui->comboBox_14->currentIndex());//串口类型
    pSettings->pSetting->setValue("COM1",ui->comboBox_15->currentIndex()+1);//串口1
    pSettings->pSetting->setValue("COM2",ui->comboBox_16->currentIndex()+1);//串口2
    pSettings->pSetting->setValue("IP1",ui->lineEdit_53->text());//串口服务器1
    pSettings->pSetting->setValue("PORT1",ui->lineEdit_54->text());//串口服务器端口1
    pSettings->pSetting->setValue("IP2",ui->lineEdit_55->text()); //串口服务器2
    pSettings->pSetting->setValue("PORT2",ui->lineEdit_56->text()); //串口服务器端口2

    //红外模式
    pQButtonGroup=new QButtonGroup (this);
    pQButtonGroup->addButton(ui->radioButton_5,0);
    pQButtonGroup->addButton(ui->radioButton_6,1);
    check=pQButtonGroup->checkedId();
    pSettings->pSetting->setValue("AlarmModel",check);
    pQButtonGroup->deleteLater();

    pSettings->pSetting->endGroup();
    //channel1----------

    //channel2----------
    pSettings->pSetting->beginGroup("Channel2");
    pSettings->pSetting->setValue("Before",ui->lineEdit_21->text());//前
    pSettings->pSetting->setValue("After",ui->lineEdit_22->text());//后
    pSettings->pSetting->setValue("Left",ui->lineEdit_23->text());//左
    pSettings->pSetting->setValue("Right",ui->lineEdit_24->text());//右
    pSettings->pSetting->setValue("Plate",ui->lineEdit_25->text());//车牌
    pSettings->pSetting->setValue("ComPortType",ui->comboBox_5->currentIndex());//串口类型
    pSettings->pSetting->setValue("COM1",ui->comboBox_6->currentIndex()+1);//串口1
    pSettings->pSetting->setValue("COM2",ui->comboBox_7->currentIndex()+1);//串口2
    pSettings->pSetting->setValue("IP1",ui->lineEdit_26->text());//串口服务器1
    pSettings->pSetting->setValue("PORT1",ui->lineEdit_27->text());//串口服务器端口1
    pSettings->pSetting->setValue("IP2",ui->lineEdit_28->text()); //串口服务器2
    pSettings->pSetting->setValue("PORT2",ui->lineEdit_29->text()); //串口服务器端口2

    //红外模式
    pQButtonGroup=new QButtonGroup (this);
    pQButtonGroup->addButton(ui->radioButton_7,0);
    pQButtonGroup->addButton(ui->radioButton_8,1);
    check=pQButtonGroup->checkedId();
    pSettings->pSetting->setValue("AlarmModel",check);
    pQButtonGroup->deleteLater();

    pSettings->pSetting->endGroup();
    //channel2----------

    //channel3----------
    pSettings->pSetting->beginGroup("Channel3");
    pSettings->pSetting->setValue("Before",ui->lineEdit_30->text());//前
    pSettings->pSetting->setValue("After",ui->lineEdit_31->text());//后
    pSettings->pSetting->setValue("Left",ui->lineEdit_32->text());//左
    pSettings->pSetting->setValue("Right",ui->lineEdit_33->text());//右
    pSettings->pSetting->setValue("Plate",ui->lineEdit_34->text());//车牌
    pSettings->pSetting->setValue("ComPortType",ui->comboBox_8->currentIndex());//串口类型
    pSettings->pSetting->setValue("COM1",ui->comboBox_9->currentIndex()+1);//串口1
    pSettings->pSetting->setValue("COM2",ui->comboBox_10->currentIndex()+1);//串口2
    pSettings->pSetting->setValue("IP1",ui->lineEdit_35->text());//串口服务器1
    pSettings->pSetting->setValue("PORT1",ui->lineEdit_36->text());//串口服务器端口1
    pSettings->pSetting->setValue("IP2",ui->lineEdit_37->text()); //串口服务器2
    pSettings->pSetting->setValue("PORT2",ui->lineEdit_38->text()); //串口服务器端口2

    //红外模式
    pQButtonGroup=new QButtonGroup (this);
    pQButtonGroup->addButton(ui->radioButton_9,0);
    pQButtonGroup->addButton(ui->radioButton_10,1);
    check=pQButtonGroup->checkedId();
    pSettings->pSetting->setValue("AlarmModel",check);
    pQButtonGroup->deleteLater();

    pSettings->pSetting->endGroup();
    //channel3----------

    //channel4----------
    pSettings->pSetting->beginGroup("Channel4");
    pSettings->pSetting->setValue("Before",ui->lineEdit_39->text());//前
    pSettings->pSetting->setValue("After",ui->lineEdit_40->text());//后
    pSettings->pSetting->setValue("Left",ui->lineEdit_41->text());//左
    pSettings->pSetting->setValue("Right",ui->lineEdit_42->text());//右
    pSettings->pSetting->setValue("Plate",ui->lineEdit_43->text());//车牌
    pSettings->pSetting->setValue("ComPortType",ui->comboBox_11->currentIndex());//串口类型
    pSettings->pSetting->setValue("COM1",ui->comboBox_12->currentIndex()+1);//串口1
    pSettings->pSetting->setValue("COM2",ui->comboBox_13->currentIndex()+1);//串口2
    pSettings->pSetting->setValue("IP1",ui->lineEdit_44->text());//串口服务器1
    pSettings->pSetting->setValue("PORT1",ui->lineEdit_45->text());//串口服务器端口1
    pSettings->pSetting->setValue("IP2",ui->lineEdit_46->text()); //串口服务器2
    pSettings->pSetting->setValue("PORT2",ui->lineEdit_47->text()); //串口服务器端口2

    //红外模式
    pQButtonGroup=new QButtonGroup (this);
    pQButtonGroup->addButton(ui->radioButton_11,0);
    pQButtonGroup->addButton(ui->radioButton_12,1);
    check=pQButtonGroup->checkedId();
    pSettings->pSetting->setValue("AlarmModel",check);
    pQButtonGroup->deleteLater();

    pSettings->pSetting->endGroup();
    //channel4----------
}

void SettingWidget::readINI()
{
    ui->comboBox->setCurrentIndex(pSettings->pSetting->value("MAIN/ChannelNumber",1).toInt()-1);

    bool check=pSettings->pSetting->value("MAIN/DataCheck",0).toBool();
    check?ui->radioButton_2->setChecked(true):ui->radioButton->setChecked(true);

    check=pSettings->pSetting->value("MAIN/AgreementType",0).toBool();
    check?ui->radioButton_4->setChecked(true):ui->radioButton_3->setChecked(true);

    ui->lineEdit_10->setText(pSettings->pSetting->value("MAIN/ServerIP","127.0.0.1").toString());
    ui->lineEdit_11->setText(pSettings->pSetting->value("MAIN/ServerPort",55555).toString());
    ui->lineEdit_12->setText(pSettings->pSetting->value("MAIN/ClientIp","127.0.0.1").toString());
    ui->lineEdit_13->setText(pSettings->pSetting->value("MAIN/ClientPort",55555).toString());
    ui->lineEdit_14->setText(pSettings->pSetting->value("MAIN/ImagePath","C:\\images\\").toString());

    ui->lineEdit_15->setText(pSettings->pSetting->value("FTP/Ip","127.0.0.1").toString());
    ui->lineEdit_16->setText(pSettings->pSetting->value("FTP/Port",21).toString());
    ui->lineEdit_17->setText(pSettings->pSetting->value("FTP/Name","user").toString());
    ui->lineEdit_18->setText(pSettings->pSetting->value("FTP/Password","admin123").toString());
    ui->lineEdit_19->setText(pSettings->pSetting->value("FTP/LocalImagePath","C:\\images\\").toString());
    ui->lineEdit_20->setText(pSettings->pSetting->value("FTP/RemoteImagePath","C:\\images\\").toString());

    ui->lineEdit_48->setText(pSettings->pSetting->value("Channel1/Before","192.168.1.100").toString());
    ui->lineEdit_49->setText(pSettings->pSetting->value("Channel1/After","192.168.1.101").toString());
    ui->lineEdit_50->setText(pSettings->pSetting->value("Channel1/Left","192.168.1.102").toString());
    ui->lineEdit_51->setText(pSettings->pSetting->value("Channel1/Right","192.168.1.103").toString());
    ui->lineEdit_52->setText(pSettings->pSetting->value("Channel1/Plate","192.168.1.104").toString());
    ui->comboBox_14->setCurrentIndex(pSettings->pSetting->value("Channel1/ComPortType",0).toInt());
    ui->comboBox_15->setCurrentIndex(pSettings->pSetting->value("Channel1/COM1",1).toInt()-1);
    ui->comboBox_16->setCurrentIndex(pSettings->pSetting->value("Channel1/COM2",2).toInt()-1);
    ui->lineEdit_53->setText(pSettings->pSetting->value("Channel1/IP1","192.168.1.105").toString());
    ui->lineEdit_54->setText(pSettings->pSetting->value("Channel1/PORT1",1).toString());
    ui->lineEdit_55->setText(pSettings->pSetting->value("Channel1/IP2","192.168.1.105").toString());
    ui->lineEdit_56->setText(pSettings->pSetting->value("Channel1/PORT2",2).toString());
    check=pSettings->pSetting->value("Channel1/AlarmModel",0).toBool();
    check?ui->radioButton_6->setChecked(true):ui->radioButton_5->setChecked(true);

    ui->lineEdit_21->setText(pSettings->pSetting->value("Channel2/Before","192.168.1.110").toString());
    ui->lineEdit_22->setText(pSettings->pSetting->value("Channel2/After","192.168.1.111").toString());
    ui->lineEdit_23->setText(pSettings->pSetting->value("Channel2/Left","192.168.1.112").toString());
    ui->lineEdit_24->setText(pSettings->pSetting->value("Channel2/Right","192.168.1.113").toString());
    ui->lineEdit_25->setText(pSettings->pSetting->value("Channel2/Plate","192.168.1.114").toString());
    ui->comboBox_5->setCurrentIndex(pSettings->pSetting->value("Channel2/ComPortType",0).toInt());
    ui->comboBox_6->setCurrentIndex(pSettings->pSetting->value("Channel2/COM1",3).toInt()-1);
    ui->comboBox_7->setCurrentIndex(pSettings->pSetting->value("Channel2/COM2",4).toInt()-1);
    ui->lineEdit_26->setText(pSettings->pSetting->value("Channel2/IP1","192.168.1.115").toString());
    ui->lineEdit_27->setText(pSettings->pSetting->value("Channel2/PORT1",1).toString());
    ui->lineEdit_28->setText(pSettings->pSetting->value("Channel2/IP2","192.168.1.115").toString());
    ui->lineEdit_29->setText(pSettings->pSetting->value("Channel2/PORT2",2).toString());
    check=pSettings->pSetting->value("Channel2/AlarmModel",0).toBool();
    check?ui->radioButton_8->setChecked(true):ui->radioButton_7->setChecked(true);

    ui->lineEdit_30->setText(pSettings->pSetting->value("Channel3/Before","192.168.1.120").toString());
    ui->lineEdit_31->setText(pSettings->pSetting->value("Channel3/After","192.168.1.121").toString());
    ui->lineEdit_32->setText(pSettings->pSetting->value("Channel3/Left","192.168.1.122").toString());
    ui->lineEdit_33->setText(pSettings->pSetting->value("Channel3/Right","192.168.1.123").toString());
    ui->lineEdit_34->setText(pSettings->pSetting->value("Channel3/Plate","192.168.1.124").toString());
    ui->comboBox_8->setCurrentIndex(pSettings->pSetting->value("Channel3/ComPortType",0).toInt());
    ui->comboBox_9->setCurrentIndex(pSettings->pSetting->value("Channel3/COM1",5).toInt()-1);
    ui->comboBox_10->setCurrentIndex(pSettings->pSetting->value("Channel3/COM2",6).toInt()-1);
    ui->lineEdit_35->setText(pSettings->pSetting->value("Channel3/IP1","192.168.1.125").toString());
    ui->lineEdit_36->setText(pSettings->pSetting->value("Channel3/PORT1",1).toString());
    ui->lineEdit_37->setText(pSettings->pSetting->value("Channel3/IP2","192.168.1.125").toString());
    ui->lineEdit_38->setText(pSettings->pSetting->value("Channel3/PORT2",2).toString());
    check=pSettings->pSetting->value("Channel3/AlarmModel",0).toBool();
    check?ui->radioButton_10->setChecked(true):ui->radioButton_9->setChecked(true);

    ui->lineEdit_39->setText(pSettings->pSetting->value("Channel4/Before","192.168.1.130").toString());
    ui->lineEdit_40->setText(pSettings->pSetting->value("Channel4/After","192.168.1.131").toString());
    ui->lineEdit_41->setText(pSettings->pSetting->value("Channel4/Left","192.168.1.132").toString());
    ui->lineEdit_42->setText(pSettings->pSetting->value("Channel4/Right","192.168.1.133").toString());
    ui->lineEdit_43->setText(pSettings->pSetting->value("Channel4/Plate","192.168.1.134").toString());
    ui->comboBox_11->setCurrentIndex(pSettings->pSetting->value("Channel4/ComPortType",0).toInt());
    ui->comboBox_12->setCurrentIndex(pSettings->pSetting->value("Channel4/COM1",7).toInt()-1);
    ui->comboBox_13->setCurrentIndex(pSettings->pSetting->value("Channel4/COM2",8).toInt()-1);
    ui->lineEdit_44->setText(pSettings->pSetting->value("Channel4/IP1","192.168.1.135").toString());
    ui->lineEdit_45->setText(pSettings->pSetting->value("Channel4/PORT1",1).toString());
    ui->lineEdit_46->setText(pSettings->pSetting->value("Channel4/IP2","192.168.1.135").toString());
    ui->lineEdit_47->setText(pSettings->pSetting->value("Channel4/PORT2",2).toString());
    check=pSettings->pSetting->value("Channel4/AlarmModel",0).toBool();
    check?ui->radioButton_12->setChecked(true):ui->radioButton_11->setChecked(true);

    //初始写入参数
    this->writeINI();
}

void SettingWidget::addComboxItem()
{
    QStringList comList;
    for (int i=1;i<=50;i++) {
        comList.append(tr("COM%1").arg(i));
    }
    ui->comboBox_15->addItems(comList);
    ui->comboBox_16->addItems(comList);
    ui->comboBox_6->addItems(comList);
    ui->comboBox_7->addItems(comList);
    ui->comboBox_9->addItems(comList);
    ui->comboBox_10->addItems(comList);
    ui->comboBox_12->addItems(comList);
    ui->comboBox_13->addItems(comList);
}

void SettingWidget::initCamerIpList()
{
    camerIDMap[10]=pSettings->pSetting->value("Channel1/Before").toString();
    camerIDMap[11]=pSettings->pSetting->value("Channel1/After").toString();
    camerIDMap[12]=pSettings->pSetting->value("Channel1/Left").toString();
    camerIDMap[13]=pSettings->pSetting->value("Channel1/Right").toString();
    camerIDMap[14]=pSettings->pSetting->value("Channel1/Plate").toString();

    camerIDMap[20]=pSettings->pSetting->value("Channel2/Before").toString();
    camerIDMap[21]=pSettings->pSetting->value("Channel2/After").toString();
    camerIDMap[22]=pSettings->pSetting->value("Channel2/Left").toString();
    camerIDMap[23]=pSettings->pSetting->value("Channel2/Right").toString();
    camerIDMap[24]=pSettings->pSetting->value("Channel2/Plate").toString();

    camerIDMap[30]=pSettings->pSetting->value("Channel3/Before").toString();
    camerIDMap[31]=pSettings->pSetting->value("Channel3/After").toString();
    camerIDMap[32]=pSettings->pSetting->value("Channel3/Left").toString();
    camerIDMap[33]=pSettings->pSetting->value("Channel3/Right").toString();
    camerIDMap[34]=pSettings->pSetting->value("Channel3/Plate").toString();

    camerIDMap[40]=pSettings->pSetting->value("Channel4/Before").toString();
    camerIDMap[41]=pSettings->pSetting->value("Channel4/After").toString();
    camerIDMap[42]=pSettings->pSetting->value("Channel4/Left").toString();
    camerIDMap[43]=pSettings->pSetting->value("Channel4/Right").toString();
    camerIDMap[44]=pSettings->pSetting->value("Channel4/Plate").toString();

}

void SettingWidget::initComPortList()
{
    QVector<QString> tmpQVerctor;
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel1/COM1").toString()));
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel1/COM2").toString()));
    comPortList[1]=tmpQVerctor;
    tmpQVerctor.clear();
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel2/COM1").toString()));
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel2/COM2").toString()));
    comPortList[2]=tmpQVerctor;
    tmpQVerctor.clear();
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel3/COM1").toString()));
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel3/COM2").toString()));
    comPortList[3]=tmpQVerctor;
    tmpQVerctor.clear();
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel4/COM1").toString()));
    tmpQVerctor.append(tr("COM%1").arg(pSettings->pSetting->value("Channel4/COM2").toString()));
    comPortList[4]=tmpQVerctor;
    tmpQVerctor.clear();
    tmpQVerctor.squeeze();

    //串口模式
    AlarmModel[1]=pSettings->pSetting->value("Channel1/AlarmModel").toBool();
    AlarmModel[2]=pSettings->pSetting->value("Channel2/AlarmModel").toBool();
    AlarmModel[3]=pSettings->pSetting->value("Channel3/AlarmModel").toBool();
    AlarmModel[4]=pSettings->pSetting->value("Channel4/AlarmModel").toBool();
}

void SettingWidget::on_buttonBox_clicked(QAbstractButton *button)
{
    if(ui->buttonBox->button(QDialogButtonBox::Save)==button)
    {
        this->writeINI();
        QMessageBox::about(this,"系统设置","参数保存成功");
    }
    if(ui->buttonBox->button(QDialogButtonBox::Discard)==button)
    {
        QMessageBox::warning(this,"系统设置","参数未保存");
    }
}
