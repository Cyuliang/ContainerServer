#include "picturewidget.h"
#include "ui_picturewidget.h"

PictureWidget::PictureWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PictureWidget)
{
    ui->setupUi(this);

    ui->treeWidget->setColumnWidth(1,10);//设置列宽
    ui->treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeToContents);//自动填充列宽

    pSetting=new  Setting();//加载配置

    this->setParent(parent);
    setWindowFlags(Qt::CustomizeWindowHint|Qt::FramelessWindowHint);
    this->hide();

    camerList.append("前相机");
    camerList.append("后相机");
    camerList.append("左相机");
    camerList.append("右相机");
    camerList.append("车牌");

    initListWidget();
    initCamerID();
}

PictureWidget::~PictureWidget()
{
    delete pSetting;
    delete ui;
}

void PictureWidget::initListWidget()
{
    QTreeWidgetItem *item = nullptr,*itemChild = nullptr;
    int channelNumber=pSetting->pSetting->value("MAIN/ChannelNumber").toInt();
    for(int i=1;i<channelNumber;i++)
    {
        item=new QTreeWidgetItem (ui->treeWidget);
        item->setText(0,tr("通道%1").arg(i+1));
        for(int j=0;j<5;j++)
        {
            itemChild=new QTreeWidgetItem (item);
            switch (j)
            {
            case 0:itemChild->setText(0,camerList[0]);
                break;
            case 1:itemChild->setText(0,camerList[1]);
                break;
            case 2:itemChild->setText(0,camerList[2]);
                break;
            case 3:itemChild->setText(0,camerList[3]);
                break;
            case 4:itemChild->setText(0,camerList[4]);
                break;
            }
            itemChild->setText(1,"离线");
            itemChild->setTextColor(1,QColor(255,85,0));
            itemChild->setTextAlignment(1,4);
        }
    }
}

void PictureWidget::initCamerID()
{
    //10,11,12,13,14,15,20,21,22,23,24,25......
    int i=0;
    int j=0;
    int k=10;

    QTreeWidgetItemIterator it(ui->treeWidget);
    while (*it) {
        if((*it)->parent()!=nullptr)
        {
            camerIDMap[k*j+i]=*it;
            i++;
        }
        else {
            i=0;
            j++;
        }
        ++it;
    }
}

void PictureWidget::setLabelPictureSlot(const QByteArray stream)
{
    QPixmap *map = new QPixmap();
    map->loadFromData(stream);
    ui->label->setPixmap(*map);

    delete map;
}

void PictureWidget::setCamerState(int id,bool state)
{
    if(state)
    {
        camerIDMap[id]->setText(1,tr("在线"));
        camerIDMap[id]->setTextColor(1,QColor(85,170,0));
        //camerIDMap[id]->setTextAlignment(1,4);
    }
    else
    {
        camerIDMap[id]->setText(1,tr("离线"));
        camerIDMap[id]->setTextColor(1,QColor(255,85,0));
        //camerIDMap[id]->setTextAlignment(1,4);
    }
}

void PictureWidget::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if(item->parent()!=nullptr)
    {
        int id=int(column*0);
        if(item->parent()->text(0)==tr("通道1"))
        {
            id=10+int(camerList.indexOf(item->text(0)));
        }
        if(item->parent()->text(0)==tr("通道2"))
        {
            id=20+int(camerList.indexOf(item->text(0)));
        }
        if(item->parent()->text(0)==tr("通道3"))
        {
            id=30+int(camerList.indexOf(item->text(0)));
        }
        if(item->parent()->text(0)==tr("通道4"))
        {
            id=40+int(camerList.indexOf(item->text(0)));
        }
        emit sendCommand(id);
    }
}
