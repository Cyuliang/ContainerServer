#ifndef PICTUREWIDGET_H
#define PICTUREWIDGET_H

#include <QWidget>
#include <QPixmap>
#include <QTreeWidgetItem>

#include "setting.h"

namespace Ui {
class PictureWidget;
}

class PictureWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PictureWidget(QWidget *parent = nullptr);
    ~PictureWidget();

private slots:

    ///
    /// \brief PictureWidget::on_treeWidget_itemClicked 手动抓拍图像
    /// \param item 选取的行
    /// \param column 选取的列
    ///
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

public slots:

    ///
    /// \brief PictureWidget::setLabelPicture 显示图像
    /// \param strem 图片流
    ///
    void setLabelPictureSlot(const QByteArray stream);

    ///
    /// \brief setCamerState 设置相机状态
    /// \param line 通道号
    /// \param id 相机号
    ///
    void setCamerState(int id,bool state);

signals:

    ///
    /// \brief PictureWidget::sendCommand 发送抓拍命令
    /// \param id
    ///
    void sendCommand(int id);

private:
    Ui::PictureWidget *ui;
    Setting *pSetting;

    ///
    /// \brief camerList相机名称
    ///
    QVector<QString> camerList;

    ///
    /// \brief camerIDMap相机ID
    ///
    QMap <int,QTreeWidgetItem*>camerIDMap;

private:

    ///
    /// \brief PictureWidget::initListWidget 添加设备列表
    ///
    void initListWidget();

    ///
    /// \brief initCamerList初始化相机列表
    ///
    void initCamerID();
};

#endif // PictureWidget_H
