#ifndef MAINUI_H
#define MAINUI_H

#include <iostream>
#include <QWidget>
#include <QListWidgetItem>
#include <QResizeEvent>

#include "logic.h"
#include "setting.h"
#include "getimage.h"
#include "seriallogic.h"
#include "datawidget.h"
#include "settingwidget.h"
#include "picturewidget.h"

namespace Ui {
class MainUI;
}

class MainUI : public QWidget
{
    Q_OBJECT

public:
    explicit MainUI(QWidget *parent = nullptr);
    ~MainUI() override;

    ///
    /// \brief resizeEvent重写窗口变化函数
    /// \param size窗口大小
    ///
    void resizeEvent(QResizeEvent* size) override;

signals:

    ///
    /// \brief PutCommandSignals抓拍指令
    /// \param command命令
    /// \param labelID图片ID
    ///
    void PutCommandSignals(const QString &command,int labelid);

    ///
    /// \brief clearnLabel来车清除前车图片
    ///
    void clearnLabel(int channel);

private slots:

    ///
    /// \brief on_listWidget_currentRowChanged窗口切换
    /// \param currentRow列表ID
    ///
    void on_listWidget_currentRowChanged(int currentRow);

    ///
    /// \brief sendCaptureCommandSignal 抓拍指令信号
    /// \param command 指令
    ///
    void sendCaptureCommandSlot(int id);

    ///
    /// \brief message日志流
    /// \param msg
    ///
    void message(const QString &msg);

    ///
    /// \brief LogicPutImage逻辑抓拍
    /// \param put逻辑
    /// \param channel通道
    ///
    void LogicPutImage(int put,int channel);

private:

    ///
    /// \brief initConnected初始化链接信号
    ///
    void initConnected();

    ///
    /// \brief initGetImage初始化GetImage类
    ///
    void initGetImage();

    ///
    /// \brief initSerialLogic初始化SerialLogic类
    ///
    void initSerialLogic();

    ///
    /// \brief logicCaptureCommand逻辑抓拍指令
    /// \param id相机ID
    /// \param labelID图片ID
    ///
    void logicCaptureCommand(int id, int labelID, uint sleepTime);

private:
    Ui::MainUI *ui;

    int channelCount;//通道数

    QThread *pThread;

    Logic *pLogic;
    Setting *pSetting;
    DataWidget *pDataWidget;
    SettingWidget *pSettingWidget;
    PictureWidget *pPictureWidget;

    ///
    /// \brief pGetImageList TCP类对象,命令类集
    ///
    QMap<int,GetImage*> pGetImagePutList;

    ///
    /// \brief pGetImageImgList TCP类对象,图片类集
    ///
    QMap<int,GetImage*> pGetImageImgList;

    ///
    /// \brief pSerialLogicList ComPort类对象,串口类集
    ///
    QMap<int,SerialLogic*>pSerialLogicList;  
};

#endif // MAINUI_H
