#ifndef GETIMAGE_H
#define GETIMAGE_H

#include "getimage_global.h"

#include  <QObject>
#include  <QThread>
#include  <QTimer>
#include  <QtNetwork/QSctpSocket>

class GETIMAGESHARED_EXPORT GetImage:public QObject
{
     Q_OBJECT
public:
    GetImage(QObject *parent = nullptr);
    ~GetImage()override;

private slots:

    ///
    /// \brief connected链接成功
    ///
    void connected();

    ///
    /// \brief readFortune接收数据
    ///
    void readFortune();

    ///
    /// \brief disconnected链接断开
    ///
    void disconnected();

    ///
    /// \brief displayError链接错误
    /// \param socketError链接对象
    ///
    void displayError(QAbstractSocket::SocketError socketError);

    ///
    /// \brief stateChanged链接状态
    /// \param socketState
    ///
    void stateChanged(QAbstractSocket::SocketState socketState);

private:
    QTcpSocket *tcpSocket;
    QTimer *pTimerLink;

    ///
    /// \brief id相机编号
    ///
    int id;

    QString ip;
    quint16 port;

    ///
    /// \brief jpgData图片流
    ///
    QByteArray jpgData;

    ///
    /// \brief labelID图片编号
    ///
    int labelID;

    ///
    /// \brief constratID触发相机编号。防止单台相机多个连接，同时发送多条信息。
    ///
    int constratID;

    ///
    /// \brief sleepTime延时显示图片
    ///
    uint sleepTime;

    ///
    /// \brief startLink开始链接
    ///
    void startLink();

signals:
    ///
    /// \brief camerStateSingal相机状态信号
    /// \param id相机ID
    /// \param state相机状态
    ///
    void camerStateSingal(int id,bool state);

    ///
    /// \brief pictureStreamSignal图片流信号
    /// \param stream图片流
    ///
    void putStreamSignal(const QByteArray stream);

    ///
    /// \brief pictureStreamSignal抓拍图片流信号
    /// \param stream图皮流
    /// \param put抓拍指令
    /// \param id相机ID
    ///
    void pictureStreamSignal(const QByteArray &stream, int labelid,int id);

    ///
    /// \brief message日志
    /// \param msg信息体
    ///
    void message(const QString &msg);

public:

    ///
    /// \brief init初始化相机
    /// \param ip地址
    /// \param port端口
    /// \param id编号
    ///
    void init(const QString &ip, quint16 port, int id);

    ///
    /// \brief PutCommand抓拍图片
    ///
    void PutCommand(const QString &command);

    ///
    /// \brief setLabelID设置要显示图片的窗口ID
    /// \param labelid
    ///
    void setLabelID(int labelid, int constratID, uint sleepTime);
};

#endif // GETIMAGE_H
