#include "getimage.h"

GetImage::GetImage(QObject *parent)
{
    this->setParent(parent);
    tcpSocket=new QTcpSocket(this);
    pTimerLink=new QTimer (this);

    connect(tcpSocket, &QIODevice::readyRead, this, &GetImage::readFortune);
    connect(tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),this, &GetImage::displayError);
    connect(tcpSocket,&QAbstractSocket::connected,this,&GetImage::connected);
    connect(tcpSocket,&QAbstractSocket::disconnected,this,&GetImage::disconnected);
    connect(tcpSocket,&QAbstractSocket::stateChanged,this,&GetImage::stateChanged); 

    connect(pTimerLink,&QTimer::timeout,this,&GetImage::startLink);    
}

GetImage::~GetImage()
{
    delete tcpSocket;
    delete pTimerLink;
}

void GetImage::init(const QString &ip, quint16 port,int id)
{
    this->constratID=-1;
    this->id=id;
    this->ip=ip;
    this->port=port;
    startLink();
}

void GetImage::startLink()
{
    tcpSocket->abort();
    tcpSocket->connectToHost(this->ip,this->port);
}

void GetImage::connected()
{
    emit camerStateSingal(id,true);

    emit message(tr("%1 链接成功").arg(id));

    //防止出现链接完成后,物理线路断开
    if(pTimerLink->isActive())
    {
        pTimerLink->stop();
    }
    pTimerLink->start(25000);
}

void GetImage::disconnected()
{
    emit camerStateSingal(id,false);

    //emit message(tr("%1 链接断开").arg(id));
}

void GetImage::PutCommand(const QString &command)
{
    if(tcpSocket->isValid()){
        //const char * str_data=command.toLatin1()
        tcpSocket->write((command.toLatin1()));
        tcpSocket->flush();
    }
}

void GetImage::setLabelID(int labelid, int constratID, uint sleepTime)
{
    this->sleepTime=sleepTime;
    this->constratID=constratID;
    this->labelID=labelid;
}

void GetImage::stateChanged(QAbstractSocket::SocketState socketState)
{
    emit message(tr("%1 链接状态 %2").arg(id).arg(socketState));
}

void GetImage::readFortune()
{
    tcpSocket = qobject_cast<QTcpSocket*>(sender());
    QByteArray dataStream = tcpSocket->readAll();

    if(dataStream!="\x00")
    {
        jpgData.append(dataStream);
    }

    int e=jpgData.lastIndexOf("\xFF\xD9");
    if(e!=-1)
    {
        int s=jpgData.indexOf("\xFF\xD8");
        jpgData=jpgData.mid(s,e-s+2);
        if(s!=-1)
        {
            if(constratID>0){
                QThread::msleep(sleepTime);
                //emit message(tr("%1 the 接收图片").arg(id));
                emit pictureStreamSignal(jpgData,labelID,id);
            }
            if(constratID==0){
                emit putStreamSignal(jpgData);
            }

        }

        jpgData.clear();
        labelID=0;
        constratID=-1;
    }

    if(pTimerLink->isActive())
    {
        pTimerLink->stop();
    }
    pTimerLink->start(20000);
}

//链接错误槽
void GetImage::displayError(QAbstractSocket::SocketError socketError)
{
    emit message(tr("%1 链接错误 %2").arg(id).arg(socketError));

    if(pTimerLink->isActive())
    {
        pTimerLink->stop();
    }
    pTimerLink->start(10000);
}
