#ifndef LOGIC_H
#define LOGIC_H

#include "logic_global.h"
#include <QObject>

class LOGICSHARED_EXPORT Logic:public QObject
{
    Q_OBJECT
public:
    explicit Logic(QObject *parent = nullptr);

signals:

    ///
    /// \brief setLogicPutImage红外状态抓图
    /// \param put逻辑
    /// \param channel通道号
    ///
    void setLogicPutImage(int put,int channel);


public slots:

    ///
    /// \brief serialLogicStatus逻辑状态
    /// \param status状态
    /// \param channel通道号
    /// \param model红外模式
    ///
     void serialLogicStatus(int *status, int channel,bool model);

private:

     ///
     /// \brief compareStatus比较红外状态
     /// \param before前一次状态
     /// \param after后一次状态
     /// \return
     ///
     bool compareStatus(int* before,int* after);

private:

     ///
     /// \brief tmpStatus红外信号
     ///
     int tmpStatus[5];

     int one,tow;
     bool _45G1;
     bool _22G1;
     bool _22G1_22G1;
};

#endif // LOGIC_H
