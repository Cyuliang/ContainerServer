#include "logic.h"

Logic::Logic(QObject *parent) : QObject(parent)
{
    setParent(parent);
    memset(tmpStatus,0,sizeof (int));

    _45G1=false;
    _22G1=false ;
    _22G1_22G1=false;
}

#include<iostream>
void Logic::serialLogicStatus(int *status, int channel,bool model)
{
    if(model) {
        one=0;tow=1;}
    else {
        one=1;tow=0;}

    if(!compareStatus(status,tmpStatus)){

        //clearn
        if(status[0]==one){
            if(status[1]==tow){
                if(status[3]==tow){
                    if(status[4]==tow)
                    {
                        emit setLogicPutImage(-1,channel);
                    }
                }
            }
        }

        //45G1
        if(status[0]==one){
            if(status[1]==one){
                if(status[3]==one){
                    if(status[4]==one){
                        emit setLogicPutImage(0,channel);
                        _22G1=false;
                        _45G1=true;
                    }
                }
            }
        }
        if(_45G1){
            if(status[0]==tow){
                if(status[1]==tow){
                    if(status[3]==one){
                        if(status[4]==one){
                            emit setLogicPutImage(1,channel);
                            _45G1=false;
                        }
                    }
                }
            }
        }

        //22G1
        if(status[0]==one){
            if(status[1]==one){
                if(status[3]==one){
                    if(status[4]==tow){
                        _22G1=true;
                    }
                }
            }
        }
        if(_22G1){
            if(status[0]==tow){
                if(status[1]==tow){
                    if(status[3]==one){
                        if(status[4]==one){
                            emit setLogicPutImage(2,channel);
                            _22G1=false;
                        }
                    }
                }
            }
        }

        //22G1_22G1
        if(status[0]==one){
            if(status[1]==tow){
                if(status[3]==one){
                    if(status[4]==one){
                        emit setLogicPutImage(3,channel);
                        _22G1_22G1=true;
                    }
                }
            }
        }
        if(_22G1_22G1){
            if(status[0]==tow){
                if(status[1]==tow){
                    if(status[3]==one){
                        if(status[4]==one){
                            emit setLogicPutImage(4,channel);
                            _22G1_22G1=false;
                        }
                    }
                }
            }
        }
    }

    memcpy(tmpStatus,status,5*sizeof (int));
}

bool Logic::compareStatus(int *before, int *after)
{
    for (int i=0;i<5;i++) {
        if(before[i]!=after[i]) {
            return false;
        }
    }
    return  true;
}
