#ifndef SERIALLOGIC_H
#define SERIALLOGIC_H

#include <QVector>
#include <QThread>
#include <QException>
#include <QtSerialPort/QSerialPort>

#include "seriallogic_global.h"

class SERIALLOGICSHARED_EXPORT SerialLogic:public QThread
{
    Q_OBJECT
public:
    explicit SerialLogic(QObject *parent = nullptr);
    ~SerialLogic()Q_DECL_OVERRIDE;

    ///
    /// \brief run运行线程
    ///
    virtual void run() Q_DECL_OVERRIDE;

    ///
    /// \brief startSlave设置参数,启动串口
    /// \param portName1串口1
    /// \param portName2串口2
    ///\param channel通道号
    ///
    void startSlave(const QString &portName1, const QString &portName2,int channel);

    ///
    /// \brief setAlarmModeSlot设置红外模式
    /// \param mode模式
    ///
    void setAlarmMode(bool model);

signals:

    ///
    /// \brief message日志
    /// \param msg信息体
    ///
     void message(const QString &msg);

     ///
     /// \brief logicStatus串口状态
     /// \param status
     ///
     void logicStatus(int* status,int channel,bool model);

private:

     ///
     /// \brief channel通道号
     ///
     int channel;

     ///
     /// \brief one逻辑状态位
     ///
     int one;
     int tow;

     ///
     /// \brief exit退出循环状态
     ///
     bool exit;

     ///
     /// \brief model红外模式
     ///
     bool model;

public:

     ///
     /// \brief portName传入串口号
     ///
     QVector<QString>portName;

     ///
     /// \brief status红外状态
     ///
     int status[5];
};

#endif // SERIALLOGIC_H
